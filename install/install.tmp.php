<?php

/* Copyright (c) 2007-08 Kyle Osborn, Alec Henriksen
 * phpns is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public Licence (GPL) as published by the Free
 * Software Foundation; either version 2 of the Licence, or (at your option) any
 * later version.
 * Please see the GPL at http://www.gnu.org/copyleft/gpl.html for a complete
 * understanding of what this license means and how to abide by it.
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>phpns &raquo; install</title>
	<link rel="shortcut icon" href="../images/icons/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="../themes/default/main.css" type="text/css" media="screen" />
	<script type="text/javascript">
		function expand() {
			for (var i=0; i<expand.arguments.length; i++) {
				var element = document.getElementById(expand.arguments[i]);
				element.style.display = (element.style.display == "none") ? "block" : "none";

			}
		}
		function new_window(url) {
			var newwindow;
			newwindow=window.open(url,'name','height=500,width=710,left=100,top=100,resizable=yes,scrollbars=yes,status=yes');
			if (window.focus) {
				newwindow.focus()
			}
		}
	</script>
</head>
<body class="install">
	<noscript>
		<div id="messages">
			Javascript is not enabled on this browser. This will result in decreased accessability, and possibly limited features.
		</div>
	</noscript>
	<div id="head_container">
		<h1><a href="../index.php">phpns</a></h1>
		<div id="tabs"> <!-- navigation start -->
			<ul id="top_nav">
				<li><a href="http://phpns.com" title="phpns website"><span>phpns website</span></a></li>
			</ul>
			<ul>
				<li><a href="index.php" class="current" title="phpns install index"><span>install index</span></a></li>
				<li><a href="../" title="phpns admin panel"><span>phpns admin panel</span></a></li>

			</ul>
		</div> <!-- navigation end -->
	</div>
	<div id="main_container">
		<div id="main_content">
			<?php echo "$logo"; ?>
			<h2>phpns installation</h2>
			<p>Welcome to the automatic phpns installation. Throughout this installation, we will ask you a few basic questions to get this up and running.</p>
			<?php echo "$content"; ?>
		</div>
		<div class="clear"></div>
	</div>
	<div id="copyright"> <!-- bottom notice/copyright -->
		<a href="http://phpns.alecwh.com">phpns 2.2.0</a> by <a href="http://alecwh.com">alec henriksen</a> under the GPL | <a href="index.php?tourId=phpns">Tour</a> &raquo; <a href="javascript:new_window('help.php');">Help</a>
	</div>
</body>
</html>
